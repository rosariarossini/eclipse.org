---
title: Eclipse Foundation Security Team
seo_title: Security Team | Security | Eclipse Foundation
---

## Staff Members

The Eclipse Staff Team is part of the Foundation’s effort to improve security posture both on projects and our own services. The main goal of the team is to increase security and compliance with international laws and regulations, providing members and contributors with the tools and guidelines best suited for the current landscape. \
As part of the strategy to improve security, the team may approach Eclipse Foundation project committers through the official and commonly used channels, using only the accounts appointed below.

If you notice permissions given to security personnel on any repository or shared space, be sure to check this page for updated accounts, keys and names. The team will use those permissions only when necessary and will use pull requests (or equivalent) in project repositories every time it is possible. \
You may notice the aforementioned permissions in security relevant spaces such as your project repository or the _.eclipsefdn_ repository on a GitHub organization.

No one in the security team will ever ask for your private credentials.

{{< pages/security/team type="staff" >}}


## Other Members
The individuals listed below are part of the security@eclipse-foundation.org mailing list in addition to above staff members. They consist of volunteer security experts who come from both the community and EMO (Eclipse Management Organization).

{{< pages/security/team type="other" >}}
