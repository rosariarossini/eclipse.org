---
title: "BasSys 4.0"
date: 2016-10-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/basys4_0.png"
tags: ["Standard", "Industry 4.0"]
homepage: "https://www.basys40.de/"
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["bmbf"]
eclipse_projects: ["dt.basyx"]
project_topic: "I4.0"
summary: "Provide virtual middleware for industrial automation which implements Industry 4.0 concepts."
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **BasSys 4.0**

In the BMBF-funded research project BaSys 4.0, Fraunhofer IESE is collaborating with 14 partners from the area of production technology to develop concepts and solutions for realizing digital twins as digital representatives for manufacturing. This creates a basic system for production plants that realizes efficient changeability of a manufacturing process as a central challenge of the fourth industrial revolution. The aim is to network and integrate existing technologies in such a way that Industry 4.0 applications can be realized. To this end, the project team is developing a virtual middle-ware that allows the necessary services to be provided and linked with each other. In doing so, central concepts from Industry 4.0 are being implemented, such as the digital twin in the form of the asset administration shell.


BaSys 4.0 was running from July 2016 to June 2019."
---

{{< grid/div class="container research-page-section" >}}

# Eclipse BaSyx
The [Eclipse BaSyx](https://www.eclipse.org/basyx/) project is the open source result of the German research project BaSys 4.0, which is funded by the Ministry for Education and Research (grant no. 01IS16022).  Eclipse BaSyx is the open source platform for the next generation automation. As a reference technology platform, the BaSyx project enables large and small industries, research institutes, academia, and interested persons, to participate in the fourth industrial revolution.

By providing common Industrie 4.0 components and an extendable SDK the platform accelerates the development of Industrie 4.0 solutions. The Eclipse BaSyx platform addresses challenges like changeable production to enable individualized goods, exploiting big data analytics and connecting heterogenous devices and systems while minimizing downtime and other associated costs.

Eclipse BaSyx realizes the following technologies implementing central pillars of Industrie 4.0 production architectures:

* The **Virtual Automation Bus** enables cross-network and cross-protocol peer-to-peer communication between manufacturing machines (shop floor) and the IT.

* **Asset Administration Shells** are digital representatives of production assets, i.e., their digital twins. These assets may be physical or non-physical in nature. The asset administration
shell of an asset contains sub-models providing, for example, its interface as well as status and live data.

* **Control Components** realize uniform service interfaces for devices. They separate the implementation of production services from the production processes and make the production changeable. Control components also realize more abstract services, which abstract from the details of the implementation and are therefore easier to use. Control components are realized by means of runtime environments.

{{</ grid/div>}}

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium

* ABB Ltd. und ABB-Forschungszentrum - Germany
* Bosch Rexroth AG - Germany
* DFKI GmbH - Germany
* Eclipse Foundation Europe GmbH - Germany
* Festo AG & Co KG - Germany
* FORTISS GmbH - Germany
* Fraunhofer IESE - Germany
* ITQ GmbH - Germany
* KUKA Roboter GmbH - Germany
* PSI Automotive & Industry GmbH - Germany
* RWTH Aachen - Germany
* Robert Bosch GmbH - Germany
* SMS group GmbH - Germany
* SYSGO AG - Germany
* ZF Friedrichshafen AG - Germany

{{</ grid/div>}}
{{</ grid/div>}}
