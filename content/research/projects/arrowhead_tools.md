---
title: "Arrowhead Tools"
date: 2019-05-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/arrowheadtools.png"
tags: ["System of Systems", "IoT", "Industry 4.0"]
homepage: "https://tools.arrowhead.eu/home"
facebook: "https://m.facebook.com/vah.sah.14"
linkedin: "https://www.linkedin.com/groups/5071265/"
twitter: "https://twitter.com/arrowheadeu"
youtube: "https://www.youtube.com/user/ArrowheadProject"
funding_bodies: ["horizon2020","ecsel","artemis"]
eclipse_projects: [iot.arrowhead]
project_topic: "I4.0 / CPS"
summary: "Open Source Platform for IoT and System of Systems"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **Arrowhead Tools**

The Arrowhead Tools project aims for digitalisation and automation solutions for the European industry, which will close the gaps that hinder the IT/OT integration by introducing new technologies in an open source platform for the design and run-time engineering of IoT and System of Systems.


The Arrowhead Tools project is a joint effort of 82 partners from 18 countries, proudly coordinated by Luleå University of Technology. See [here](https://www.arrowhead.eu/arrowheadtools/partners) for the full list of partners.


The Arrowhead Framework enables the design and implementation of automation systems in application domains such as production, smart cities, e-mobility, energy, and buildings. It was created to efficiently address Industry 4.0 requirements, primarily through scalable, secure, and flexible information sharing that enables system interoperability and integration. To achieve these goals, the SOA architecture abstracts each interface that exchanges information as a service. Instead of hardwiring the connections, it enables loose coupling, late binding, and lookups to discover services.

Since 2016 when the Arrowhead Framework was released, a number of other European Union and national projects have added to it. As a result, the Arrowhead Framework architecture and its reference implementation can be used to implement Industry 4.0 architectures, such as the [Reference Architecture Model for Industry 4.0 (RAMI 4.0)](https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/rami40-an-introduction.html) and the [Industrial Internet Reference Architecture (IIRA)](https://www.iiconsortium.org/IIRA.htm).

The Arrowhead Framework architecture has already been applied in:

- Industrial control systems, such as supervisory control and data acquisition (SCADA) and distributed control systems (DCSs)

- Manufacturing execution systems (MESs)

- Programmable logic controllers (PLCs)

- IoT solutions, such as building energy management, industrial gateways for smart city applications, and intelligent rock bolts for mining safety


Arrowhead Tools was running from May 2019 to June 2022."

---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Eclipse Arrowhead
The [Eclipse Arrowhead](https://projects.eclipse.org/projects/iot.arrowhead) project was created to provide long-term governance and promotion of the Arrowhead Framework, a Service Oriented Architecture (SOA) with a reference implementation for Internet of Things (IoT) interoperability that was originally developed as part of the Arrowhead Tools European research project .

To a large extent, automation is geographically local. Combining local automation with real-time and security requirements leads to the concept of self-contained local clouds. A local cloud is a private network that becomes a shell within which sensitive functionality is grouped. If the private network has a real-time, physical network layer, hard real-time performance can be realized with the local cloud.

The Eclipse Arrowhead project is based on an SOA that features loose coupling, late binding, and lookups. Together, these features enable discovery of services in operation. They also enable run-time definition of service bindings and provide autonomous service exchange operation until further notice. These capabilities are supported by the Arrowhead core systems ServiceRegistry and Orchestration. In addition, security of service exchanges requires authentication of the service consumer and authorization of the specific service consumption. This is supported by the Arrowhead core system Authorisation.

# Consortium

* LULEA TEKNISKA UNIVERSITET - SE
* AEE - INSTITUT FUR NACHHALTIGE TECHNOLOGIEN - AT
* ACCIONA CONSTRUCCION SA - ES
* ARCELIK A.S. - TR
* AVL LIST GMBH - AT
* BETTERSOLUTIONS SA - PL
* BNEARIT AB - SE
* BOLIDEN MINERAL AB - SE
* dotGIS corporation - ES
* ECLIPSE FOUNDATION EUROPE GMBH - DE
* EQUA SIMULATION AB - SE
* EUROTECH SPA - IT
* EVOLARIS NEXT LEVEL GMBH - AT
* FAGOR ARRASATE S COOP - ES
* FAGOR AUTOMATION S COOP LTDA - ES
* POLITECHNIKA GDANSKA - PL
* HONEYWELL INTERNATIONAL SRO - CZ
* IKERLAN S COOP - ES
* INFINEON TECHNOLOGIES AUSTRIA AG - AT
* INFINEON TECHNOLOGIES DRESDEN GMBH& CO KG - DE
* INFINEON TECHNOLOGIES AG - DE
* USTAV TEORIE INFORMACE A AUTOMATIZACE AV CR VVI - CZ
* INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO - PT
* CONSORZIO NAZIONALE INTERUNIVERSITARIO PER LA NANOELETTRONICA - IT
* JOTNE EPM TECHNOLOGY AS - NO
* KAI KOMPETENZZENTRUM AUTOMOBIL - UND INDUSTRIEELEKTRONIK GMBH - AT
* LINDBACKS BYGG AB - SE
* Lundqvist Trävaru AB - SE
* MONDRAGON GOI ESKOLA POLITEKNIKOA JOSE MARIA ARIZMENDIARRIETA S COOP - ES
* MONDRAGON SISTEMAS DE INFORMACION SOCIEDAD COOPERATIVA - ES
* HOGSKOLEN I OSTFOLD - NO
* PHILIPS MEDICAL SYSTEMS NEDERLAND BV - NL
* PODCOMP AB - SE
* POLITECNICO DI TORINO - IT
* SANTER REPLY SPA - IT
* KNOWLEDGE CENTRIC SOLUTIONS SL - ES
* STMICROELECTRONICS SRL - IT
* SINTEF AS - NO
* TELLU IOT AS - NO
* TECHNISCHE UNIVERSITAET DRESDEN - DE
* TECHNISCHE UNIVERSITAET KAISERSLAUTERN - DE
* UNIVERSIDAD CARLOS III DE MADRID - ES
* ULMA EMBEDDED SOLUTIONS S COOP - ES
* Kompetenzzentrum - Das Virtuelle Fahrzeug, Forschungsgesellschaft mbH - AT
* SIRRIS HET COLLECTIEF CENTRUM VAN DE TECHNOLOGISCHE INDUSTRIE - BE
* 3E NV - BE
* VOLVO LASTVAGNAR AB - SE
* ELEKTRONIKAS UN DATORZINATNU INSTITUTS - LV
* FACHHOCHSCHULE BURGENLAND GMBH - AT
* Masarykova univerzita - CZ
* VYSOKE UCENI TECHNICKE V BRNE - CZ
* CESKE VYSOKE UCENI TECHNICKE V PRAZE - CZ
* ROPARDO SRL - RO
* SAP Norway AS - NO
* AllThingsTalk NV - BE
* INSTITUTE FUR ENGINEERING DESING OF MECHATRONIC SYSTEMS UND MPLM EV - DE
* CISC SEMICONDUCTOR GMBH - AT
* INSTITUT FUER AUTOMATION UND KOMMUNIKATION E.V. MAGDEBURG - DE
* COMMISSARIAT A L ENERGIE ATOMIQUE ET AUX ENERGIES ALTERNATIVES - FR
* MAGILLEM DESIGN SERVICES SAS - FR
* TECHNEXT - FR
* Imagerie Medicale de la Plaine de France - FR
* STMICROELECTRONICS GRENOBLE 2 SAS - FR
* AITIA INTERNATIONAL INFORMATIKAI ZARTKORUEN MUKODO RT - HU
* EVOPRO INFORMATIKAI ES AUTOMATIZALASI KFT - HU
* BUDAPESTI MUSZAKI ES GAZDASAGTUDOMANYI EGYETEM - HU
* INCQUERY LABS KUTATAS-FEJLESZTESI KFT - HU
* VIRTUAL POWER SOLUTIONS SA - PT
* UNIVERSIDADE NOVA DE LISBOA - PT
* TECHNISCHE UNIVERSITAET GRAZ - AT
* UNIVERSITAT ZU LUBECK - DE
* BEIA CONSULT INTERNATIONAL SRL - RO
* TECHNISCHE UNIVERSITEIT EINDHOVEN - NL
* AIT AUSTRIAN INSTITUTE OF TECHNOLOGY GMBH - AT
* SYSTEMA SYSTEMENTWICKLUNG DIPL INF.MANFRED AUSTEN GMBH - DE
* Semantis Information Builders GmbH - DE
* ROBERT BOSCH GMBH - DE
* ASML NETHERLANDS B.V. - NL
* ICT AUTOMATISERING NEDERLAND BV - NL
* EQUA Solutions AG - CH
* Hochschule fuer Technik und Wirtschaft Dresden - DE
* BOSCH SOFTWARE INNOVATIONS GMBH - DE
* ASSYSTEM GERMANY GMBH - DE
* MONDRAGON CORPORACION COOPERATIVA SCOOP - ES
* WAPICE OY - FI
* ABB OY - FI
* Teknologian tutkimuskeskus VTT Oy - FI
* TECHNOLUTION BV - NL

{{</ grid/div>}}
{{</ grid/div>}}
