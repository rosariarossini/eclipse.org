---
title: "TRANSACT"
date: 2021-04-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/transact.png"
tags: ["CPS", "Cyber-Physcial System", "Security", "Cloud", "Edge", "Automotive"]
homepage: "https://transact-ecsel.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/transact-project/"
twitter: "https://twitter.com/TransactProject"
youtube: ""
funding_bodies: ["horizon2020", "ecsel"]
eclipse_projects: ["modeling.poosl"]
project_topic: "IoT / CPS"
summary: "Towards safe and secure distributed cyber-physical systems"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **TRANSACT**

Cyber-physical systems (CPS) are all around us, but due to today’s technical limitations and the possibility of human error, we cannot yet tap into their full potential. A more integrated and connected architecture for such systems, via edge and cloud technologies, could overcome these limitations.


The overarching goal of TRANSACT is therefore to develop a universal, distributed solution architecture for the transformation of safety-critical cyber-physical systems, from localised stand-alone systems into safe and secure distributed solutions.


To that end, TRANSACT will research distributed reference architectures for safety-critical CPS that rely on edge and cloud computing. These architectures will enable seamless mixing of on-device, edge and cloud services while assuring flexible yet safe and secure deployment of new applications, and independent releasing of edge and cloud-based components versus the on device parts. The distributed solutions will simplify CPS devices, reducing their software footprint, and consequently their Bill-of-Material (BoM) and Life Cycle Management (LCM) costs. A key element in the transformation of safety-critical CPS into distributed safety-critical CPS solutions (on-device, edge or cloud based) is that performance, safety, security, and privacy of data are guaranteed: the safeguarding of these properties gets due attention in the project.

Finally, by integrating AI based services into distributed CPS, TRANSACT will enable fast development of innovative value-based propositions and business models leading to faster market introduction in the various multi-billion euro markets targeted by the TRANSACT project.


This project was running from May 2021 to April 2024."
---


{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium
* PHILIPS MEDICAL SYSTEMS NEDERLAND BV  - NL
* TECHNISCHE UNIVERSITEIT EINDHOVEN  - NL
* PS-TECH B.V.  - NL
* VINOTION BV  - NL
* NEDERLANDSE ORGANISATIE VOOR TOEGEPAST NATUURWETENSCHAPPELIJK ONDERZOEK TNO  - NL
* Fleetonomy.ai Oy  - FI
* Teknologian tutkimuskeskus VTT Oy  - FI
* F-SECURE OYJ  - FI
* Nodeon Finland Oy  - FI
* AVL SOFTWARE AND FUNCTIONS GMBH  - DE
* ECLIPSE FOUNDATION EUROPE GMBH  - DE
* OFFIS EV  - DE
* Philips GmbH  - DE
* DENSO AUTOMOTIVE DEUTSCHLAND GMBH  - DE
* POLITECHNIKA GDANSKA  - PL
* DAC Spolka Akcyjna  - PL
* TECHNISCHE UNIVERSITAET GRAZ  - AT
* CISC SEMICONDUCTOR GMBH  - AT
* NAVTOR AS  - NO
* SIMULA RESEARCH LABORATORY AS  - NO
* INSTITUTO TECNOLOGICO DE INFORMATICA  - ES
* NUNSYS  - ES
* Kumori Systems  - ES
* Singlar Innovación, S.L.  - ES
* FUNDACIO PER A LA UNIVERSITAT OBERTA DE CATALUNYA  - ES
* FEOPS NV  - BE
* DANMARKS TEKNISKE UNIVERSITET  - DK
* Toitware ApS  - DK

{{</ grid/div>}}
{{</ grid/div>}}
