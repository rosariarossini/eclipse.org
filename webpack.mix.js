/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();

mix.setPublicPath('static/public');
mix.setResourceRoot('../');

// default styles
mix.less('./less/styles.less', 'static/public/css/styles.css');
mix.js('js/main.js', './static/public/js/main.js');

// page_css_file
mix.less('./less/page_css_file/collaborations/styles.less', 'static/public/css/collaborations-styles.css');
mix.less('./less/page_css_file/openchain/styles.less', 'static/public/css/projects-openchain.css');
mix.less('./less/page_css_file/europe/styles.less', 'static/public/css/europe-styles.css');

// eclipse.org/research
mix.less('./less/page_css_file/research/styles.less', 'static/public/css/research-styles.css');
mix.css('./node_modules/datatables.net-dt/css/jquery.dataTables.min.css', 'static/public/css/datatables.net-dt.css');
mix.js('js/known-vulnerabilities.js', './static/public/js/known-vulnerabilities.js')